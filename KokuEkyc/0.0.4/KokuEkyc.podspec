#
#  Be sure to run `pod spec lint KokuEkyc.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

#1
  spec.platform = :ios
  spec.ios.deployment_target = '12.0'
  spec.name         = "KokuEkyc"
  spec.summary      = "ekyc for public use from Koku"
  spec.requires_arc = true

#2
  spec.version      = "0.0.4"

#3
    spec.license      = { :type => "MIT", :file => "LICENSE" }

    #4
    spec.author             = { "Chamara Thennakoon" => "chamara.p@koku.io" }

    #5
    spec.homepage     = "https://bitbucket.org/koku_sample/ekyc.sdk.ios.publicbinary"

    # 6 - Replace this URL with your own Git URL from "Quick Setup"
    spec.source = { :git => "https://bitbucket.org/koku_sample/ekyc.sdk.ios.publicbinary.git",
             :tag => "#{spec.version}" }

    # 7
    spec.framework = "UIKit"
    spec.framework = "AVFoundation"
    spec.framework = "CoreMotion"
    spec.framework = "CoreTelephony"
    spec.framework = "SystemConfiguration"
    # spec.framework = "AAILivenessSDK"
    # spec.dependency 'Alamofire', '~> 4.9.0'
    # spec.dependency 'SwiftyJSON', '~> 4.2.0'
    # spec.dependency 'DropDown'
    # spec.dependency 'IQKeyboardManagerSwift'
    # spec.dependency 'SVProgressHUD'
    # spec.dependency 'JVFloatLabeledTextField'
    spec.vendored_frameworks = 'ekyc.framework'

    # spec.static_framework = true

    # 8
    # spec.source_files = 'ekyc/**/*.{swift,h,m, mm}'
    # spec.module_map = 'ekyc/AAILivenessSDK/AAILivenessSDK.framework/Modules/module.modulemap'
    # spec.public_header_files = 'ekyc/AAILivenessSDK/AAILivenessSDK.framework/Headers/*.h'
    # spec.public_header_files = 'ekyc/ekyc.h, ekyc/AAILivenessSDK/AAILivenessSDK.framework/Headers/*.h, ekyc/AAILivenessSDK/AAILiveness/*.h'
    # spec.public_header_files = 'ekyc/ekyc.h'

    # spec.xcconfig            = { 'FRAMEWORK_SEARCH_PATHS' => 'ekyc/AAILivenessSDK' }

    # framework_supplement = "Framework/#{s.name}"
    # umbrella_header    = "#{framework_supplement}/#{s.name}.h"
    # s.module_name      = s.name
    # s.module_map       = "#{framework_supplement}/Map.modulemap"

    # spec.preserve_paths = 'ekyc/AAILivenessSDK/AAILivenessSDK.framework'


    # spec.module_name = 'AAILivenessSDK'
    # spec.prefix_header_file = false
    # spec.header_mappings_dir = "ekyc"
    # spec.public_header_files = "ekyc/#{spec.module_name}/AAILiveness/**/*.h", "ekyc/AAILivenessSDK/AAILivenessSDK.framework/Headers/*.h", "ekyc/ekyc.h"
    # spec.private_header_files = "ekyc/#{spec.module_name}/AAILiveness/**/*.h", "ekyc/AAILivenessSDK/AAILivenessSDK.framework/Headers/*.h", "ekyc/ekyc.h"
    # spec.source_files = "ekyc/#{spec.module_name}/**/*.{h,m}", "ekyc/**/*.{swift}", "ekyc/AAILivenessSDK/AAILivenessSDK.framework/Headers/*.h"
    # spec.xcconfig = {
    #   'HEADER_SEARCH_PATHS' => "\"${PODS_TARGET_SRCROOT}/Sources/#{spec.module_name}/AAILiveness\"",
    # }

    # spec.module_name = 'AAILivenessSDK'
    # spec.prefix_header_file = false
    # spec.header_mappings_dir = "ekyc"
    # spec.public_header_files = "ekyc/AAILivenessSDK/AAILivenessSDK.framework/Headers/*.h"
    # spec.private_header_files = "ekyc/#{spec.module_name}/AAILiveness/**/*.h", "ekyc/AAILivenessSDK/AAILivenessSDK.framework/Headers/*.h"
    # spec.source_files = "ekyc/#{spec.module_name}/**/*.{h,m}", "ekyc/**/*.{swift}", "ekyc/AAILivenessSDK/AAILivenessSDK.framework/Headers/*.h"
    # spec.xcconfig = {
    #   'HEADER_SEARCH_PATHS' => "\"${PODS_TARGET_SRCROOT}/Sources/#{spec.module_name}/AAILiveness\"",
    # }

#     spec.script_phase = {:name=> 'ekyc', :script=> 'UNIVERSAL_OUTPUTFOLDER=${BUILD_DIR}/${CONFIGURATION}-universal
# mkdir -p "${UNIVERSAL_OUTPUTFOLDER}"
# xcodebuild BITCODE_GENERATION_MODE=bitcode OTHER_CFLAGS="-fembed-bitcode" -target "${PROJECT_NAME}" ONLY_ACTIVE_ARCH=NO -configuration ${CONFIGURATION} -sdk iphoneos BUILD_DIR="${BUILD_DIR}" BUILD_ROOT="${BUILD_ROOT}" clean build
# xcodebuild BITCODE_GENERATION_MODE=bitcode OTHER_CFLAGS="-fembed-bitcode" -target "${PROJECT_NAME}" ONLY_ACTIVE_ARCH=NO -configuration ${CONFIGURATION} -sdk iphonesimulator BUILD_DIR="${BUILD_DIR}" BUILD_ROOT="${BUILD_ROOT}" clean build
# cp -R "${BUILD_DIR}/${CONFIGURATION}-iphoneos/${PROJECT_NAME}.framework" "${UNIVERSAL_OUTPUTFOLDER}/"
# cp -R "${BUILD_DIR}/${CONFIGURATION}-iphonesimulator/${PROJECT_NAME}.framework/Modules/${PROJECT_NAME}.swiftmodule/." "${UNIVERSAL_OUTPUTFOLDER}/${PROJECT_NAME}.framework/Modules/${PROJECT_NAME}.swiftmodule"
# lipo -create -output "${UNIVERSAL_OUTPUTFOLDER}/${PROJECT_NAME}.framework/${PROJECT_NAME}" "${BUILD_DIR}/${CONFIGURATION}-iphonesimulator/${PROJECT_NAME}.framework/${PROJECT_NAME}" "${BUILD_DIR}/${CONFIGURATION}-iphoneos/${PROJECT_NAME}.framework/${PROJECT_NAME}"
# cp -R "${UNIVERSAL_OUTPUTFOLDER}/${PROJECT_NAME}.framework" "${PROJECT_DIR}"
# # Step 6. Convenience step to open the project\'s directory in Finder
# open "${PROJECT_DIR}"
# ' }

    # spec.xcconfig = {
    #   'OTHER_LDFLAGS' => '-ObjC'
    # }
    # spec.ios.vendored_frameworks = 'ekyc/AAILivenessSDK/AAILivenessSDK.framework'
    # s.preserve_paths      = 'AAILivenessSDK/AAILivenessSDK.framework'
    # s.frameworks          = 'Foundation', 'AAILivenessSDK'
    # s.xcconfig            = { 'FRAMEWORK_SEARCH_PATHS' => '$(SRCROOT)/AAILivenessSDK/' }
    # s.public_header_files = 'AAILivenessSDK.framework/Headers/*.h', 'SourceCode/*.h'

    # spec.subspec 'AAILivenessSDK' do |ss|
   # ss.source_files = 'ekyc/AAILivenessSDK/AAILivenessSDK.framework/Headers/*.h'
   # ss.public_header_files = 'ekyc/AAILivenessSDK/AAILivenessSDK.framework/Headers/*.h'
   # ss.module_map = 'ekyc/AAILivenessSDK/module.modulemap'
   # ss.frameworks = 'CFNetwork', 'Security', 'SystemConfiguration', 'JavaScriptCore'
   # ss.vendored_frameworks = 'ekyc/AAILivenessSDK/AAILivenessSDK.framework'
   # ss.xcconfig = {
   #   'OTHER_LDFLAGS' => '-ObjC'
   # }
   # ss.preserve_paths = 'ekyc/AAILivenessSDK/AAILivenessSDK.framework'
   # end

    # 9
    # spec.resources = "ekyc/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"


    # 10
    spec.swift_version = "4.2"
end
